/*La función Print llama a Onclick de HTML, entre paréntesis están las propiedades
a las que llamamos. La variable firstOperant nos permite introducir un núm. en 
la primera caja y contatenamos los números.*/
function print(value, operator){

    var firstOperant = document.getElementById("entryNumber").innerHTML;

    if(firstOperant == "0" && value != "."){ //limitamos el poder escribir más de un 0 como primera cifra//
        return false; //si el primer valor es 0, se para la ejecución//
    }

    if(firstOperant == "." && value != "0"){ //se limita el poder escribir más de un decimal//
        return false; //si el primer valor es un decimal, se para la ejecución.//
    }

    firstOperant += value; //suma entryNumber+exitNumber//
    
    if(operator){
        document.getElementById("exitNumber").innerHTML = firstOperant;//si hacemos clic en el operador entonces el número pasa de entryNumber a exitNumber//
        document.getElementById("entryNumber").innerHTML = "";
    }else{
        document.getElementById("entryNumber").innerHTML = firstOperant;
    }
}
/*En la variable de Operation, coge ambos valores (entryNumber y exitNumber)
y los junta sin hacer ningun cálculo. Eval es lo que nos permite poder hacer
el cálculo, se pone entre paréntesis el nombre de la variable que contiene
ambos ''document'' que en este caso es la variable ''operation''*/

function result(){
    var operation = document.getElementById("exitNumber").innerHTML + document.getElementById("entryNumber").innerHTML; //concatena
    document.getElementById("entryNumber").innerHTML = eval(operation);//permite calcular en la parte de abajo//
    document.getElementById("exitNumber").innerHTML = eval(operation); //permite calcular en la parte de arriba//
}

function borradoParcial(value){
    document.getElementById("entryNumber").innerHTML = "";//*borra solo la parte de abajo//
}

function borrarTodo(value){//borra ambas partes//
    document.getElementById("entryNumber").innerHTML = "";
    document.getElementById("exitNumber").innerHTML = "";
}

function borradoUltimoNum(value){
    var operator = document.getElementById("entryNumber").innerHTML;
    var borrar = operator.substring(0, operator.length - 1); //borra la última cifra tecleada//
    operator = borrar; //lo que hay en la var operator activa la var borrar y borra la última cifra
    document.getElementById("entryNumber").innerHTML = operator; //imprime el número sin la cifra borrada
}



